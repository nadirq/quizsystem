<?php

/**
 * Class WebApp
 */
class WebApp
{
    private static $_instance;
    private        $_registry = [];

    /**
     * Инициализация приложения
     *
     * @param $config
     */
    public static function createInstance($config)
    {
        self::$_instance = new self($config);
        Router::process();
    }

    private function registry($config)
    {
        $db = $config['db'];

        $this->_registry['db'] = new PDO(
            'mysql:dbname=' . $db['dbname'] . ';host=' . $db['host'],
            $db['user'],
            $db['passwd']
        );
    }

    public static function instance()
    {
        return self::$_instance;
    }

    private function __construct($config)
    {
        $this->registry($config);
    }

    private function __wakeup()
    {
    }

    private function __clone()
    {
    }

    public function __get($name)
    {
        if (isset($this->_registry[$name])) {
            return $this->_registry[$name];
        }
    }
}