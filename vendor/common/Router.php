<?php

/**
 * Class Router
 */
class Router
{
    /**
     * Обработка роутов
     *
     * @throws Exception
     */
    public static function process()
    {
        $route = [];
        parse_str($_SERVER['QUERY_STRING'], $route);

        // если роут не определен, выставляем роут по умолчанию
        $route = !empty($route['route']) ? $route['route'] : 'quiz/start';

        $routes = explode('/', $route);

        // $routes[0] всегда будет определен
        $controller_name = ucfirst($routes[0]);
        $action_name     = !empty($routes[1]) ? $routes[1] : 'index';
        $controller_name .= 'Controller';
        $controller = new $controller_name;

        if (method_exists($controller, $action_name)) {
            $controller->$action_name();
        } else {
            throw new Exception('action ' . $action_name . ' is not found in ' . get_class($controller));
        }
    }
}