<?php

/**
 * Class Model
 *
 * TODO сделать связи в моделях. Это позволит избежать сложной реализации при действиях со связанными таблицами
 */
abstract class Model
{
    protected static $_model;

    protected $_attributes = []; // список полей таблицы

    protected $_is_new_record = false; // новая запись или нет

    abstract public function tableName();

    /**
     * Сохранение модели
     *
     * TODO вынести доступ к первичному ключу через отдельный метод
     */
    public function save()
    {
        $set = ' SET ';

        // формируем запрос
        foreach ($this->_attributes as $key => $value) {
            if ($key !== 'id' || !empty($value)) {
                $set .= "`$key` = '$value', ";
            }
        }

        // удаляем последнюю запятую
        $set = preg_replace('/(.*),\s$/', '${1}', $set);

        if ($this->_is_new_record) {
            $this->id = null;
            $query    = 'INSERT INTO ' . $this->tableName() . $set;
        } else {
            $query = 'UPDATE ' . $this->tableName() . $set . ' WHERE `id`=' . $this->id;
        }

        $this->processQuery($query);
    }

    /**
     * Удаление строки по условиям.
     * В базе данных настроены внешние ключи, поэтому не нужно рекурсивно удалять все связанные записи
     *
     * @param array $conditions_array
     */
    public function delete(array $conditions_array = [])
    {
        $where = " WHERE ";

        if (empty($conditions_array)) {
            $where .= "`id` = $this->id";
        } else {
            foreach ($conditions_array as $key => $value) {
                $where .= "`$key`='$value' AND ";
            }

            $where = preg_replace('/(.*)AND\s$/', '${1}', $where);
        }

        $query = 'DELETE from ' . $this->tableName() . $where;

        $this->processQuery($query);
    }

    /**
     * Метод нужен только для семантической адекватности
     * Через инстанс класса мы можем сделать выборку существующих моделей, в то время как синтаксис вида
     * $model = new Model() подразумевает о создании новой модели, т.е. строки в таблице
     *
     * @param string $className
     *
     * @return Model
     */
    public static function instance($className = __CLASS__)
    {
        return new $className();
    }

    public function __construct()
    {
        $this->_is_new_record = true;
    }

    /**
     * Выбрать все записи в таблице
     *
     * @return Model[]
     */
    public function findAll()
    {
        $query = 'SELECT * FROM ' . $this->tableName();

        return $this->processSelectQuery($query);
    }

    /**
     * Выбрать записи по условиям
     *
     * @param array $conditions_array На вход подается массив вида ключ => значения
     *                                Если приходит массив вида ключ => массив значений,
     *                                то выполняется условие ИЛИ
     *
     * @return Model[]
     */
    public function findByAttributes($conditions_array = [])
    {
        $where = '';

        if (!empty($conditions_array)) {
            $where = ' WHERE ';
            foreach ($conditions_array as $field => $condition) {
                if (is_array($condition)) {
                    $in = "`{$field}` in (";

                    foreach ($condition as $c) {
                        $in .= "'{$c}', ";
                    }

                    // удаляем лишнюю запятую
                    $in = preg_replace('/(.*),\s$/', '${1}', $in);
                    $in .= ') AND ';
                    $where .= $in;
                } else {
                    $where .= "`{$field}` = '{$condition}' AND ";
                }
            }

            $where = preg_replace('/(.*)AND\s$/', '${1}', $where);
        }

        $query = 'SELECT * FROM ' . $this->tableName() . $where;

        return $this->processSelectQuery($query);
    }

    /**
     * Выполнение запроса выборки
     *
     * @param $query
     *
     * @return Model[]
     */
    protected function processSelectQuery($query)
    {
        /**
         * @var PDOStatement $stmt
         */
        $stmt        = WebApp::instance()->db->query($query);
        $models_data = $stmt->fetchAll(PDO::FETCH_ASSOC);

        return $this->prepareModels($models_data);
    }

    /**
     * Выполняет запрос на удаление/вставку/обновление
     *
     * @param string $query
     */
    protected function processQuery($query)
    {
        WebApp::instance()->db->query($query);
    }

    /**
     * Подготовка моделей на основе данных выборки
     *
     * @param $models_data
     *
     * @return Model[]
     */
    protected function prepareModels($models_data)
    {
        $models = [];

        foreach ($models_data as $model_data) {
            $class_name = get_class($this);
            /**
             * @var Model $model
             */
            $model                 = new $class_name();
            $model->_is_new_record = false;
            $model->setAttributes($model_data);
            $models[] = $model;
        }

        return $models;
    }

    /**
     * Возвращает только те значения, которые есть в $_attributes
     *
     * @param string $name
     *
     * @return mixed|null
     */
    public function __get($name)
    {
        if (isset($this->_attributes[$name])) {
            return $this->_attributes[$name];
        }

        return null;
    }

    /**
     * Устанавливает только те значения, которые есть в $_attributes
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if (isset($this->_attributes[$name])) {
            $this->_attributes[$name] = $value;
        }
    }

    /**
     * @param array $params
     */
    public function setAttributes(array $params)
    {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->_attributes;
    }
}