<?php

/**
 * Class Question
 *
 * @property int    $id
 * @property int    $quiz_id
 * @property string $question
 * @property int    $type
 * @property int    $required
 *
 */
class Question extends Model
{
    const TYPE_RADIO    = 0;
    const TYPE_CHECKBOX = 1;

    protected $_attributes = [
        'id'       => '',
        'quiz_id'  => '',
        'question' => '',
        'type'     => '',
        'required' => '',
    ];

    public function tableName()
    {
        return 'question';
    }

    /**
     * @param string $className
     *
     * @return Question
     */
    public static function instance($className = __CLASS__)
    {
        return parent::instance($className);
    }
}