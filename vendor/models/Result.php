<?php

/**
 * Class Result
 *
 * @property string $id
 * @property int    $quiz_id
 * @property int    $question_id
 * @property int    $answer_id
 */
class Result extends Model
{
    protected $_attributes = [
        'id'          => '',
        'quiz_id'     => '',
        'question_id' => '',
        'answer_id'   => '',
    ];

    public function tableName()
    {
        return 'result';
    }

    /**
     * @param string $className
     *
     * @return Result
     */
    public static function instance($className = __CLASS__)
    {
        return parent::instance($className);
    }

    /**
     * Сохранение  результатов голосования
     *
     * @param array $post
     *
     * @return array
     */
    public function savePoll($post)
    {
        $errors = $this->validate($post);
        $id     = $this->gen_uuid();

        if (!empty($errors)) {
            return $errors;
        }

        foreach ($post['Question'] as $question_id => $question) {
            if (empty($question['Answers'])) {
                continue;
            }

            foreach ($question['Answers'] as $answer_id) {
                $result_model              = new self();
                $result_model->id          = $id;
                $result_model->quiz_id     = $post['Quiz']['id'];
                $result_model->question_id = $question_id;
                $result_model->answer_id   = $answer_id;
                $result_model->save();
            }
        }

        return ['result' => ['Результаты опроса сохранены']];
    }

    /**
     * Валидация рзульатов опроса
     *
     * @param array $post
     *
     * @return array
     */
    protected function validate($post)
    {
        $quiz      = isset($post['Quiz']) ? $post['Quiz'] : false;
        $questions = isset($post['Question']) && !empty($post['Question']) ? $post['Question'] : false;

        if (!$quiz || !$questions) {
            return ['errors' => ['Неизвестная ошибка']];
        }

        foreach ($questions as $question) {
            if ($question['required'] == 1 && empty($question['Answers'])) {
                return ['errors' => ['Нужно ответить на все обязательные вопросы']];
            }
        }

        return [];
    }

    /**
     * Генерируем уникальный UUID, который будем использовать для идентификации результата опроса
     *
     * @return string
     */
    protected function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                       mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                       mt_rand(0, 0xffff),
                       mt_rand(0, 0x0fff) | 0x4000,
                       mt_rand(0, 0x3fff) | 0x8000,
                       mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}