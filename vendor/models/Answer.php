<?php

/**
 * Class Answer
 *
 * @property int    $id
 * @property int    $question_id
 * @property string $answer
 *
 */
class Answer extends Model
{
    protected $_attributes = [
        'id'          => '',
        'question_id' => '',
        'answer'      => '',
    ];

    public function tableName()
    {
        return 'answer';
    }

    /**
     * @param string $className
     *
     * @return Answer
     */
    public static function instance($className = __CLASS__)
    {
        return parent::instance($className);
    }
}