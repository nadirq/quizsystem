<?php

/**
 * Class Quiz
 *
 * @property int    $id
 * @property string $name
 * @property int    $status
 *
 */
class Quiz extends Model
{
    const TYPE_DRAFT  = 0;
    const TYPE_ACTIVE = 1;
    const TYPE_CLOSED = 2;

    const TYPES = [
        0 => 'Черновик',
        1 => 'Активный опрос',
        2 => 'Закрытый опрос',
    ];

    protected $_attributes = [
        'id'     => '',
        'name'   => '',
        'status' => '',
    ];

    public function tableName()
    {
        return 'quiz';
    }

    /**
     * Логика сохранения опроса
     */
    public function saveDraft($post)
    {
        $errors = $this->validate($post);

        if (!empty($errors)) {
            return $errors;
        }

        $quiz_id = $post['Quiz']['quiz_id'];

        // обновление существующего опроса или создание нового
        if (!empty($quiz_id)) {
            $quiz = Quiz::instance()->findByAttributes(['id' => $quiz_id])[0];
            $quiz->setAttributes($post['Quiz']);
        } else {
            $quiz         = new Quiz();
            $quiz->name   = $post['Quiz']['name'];
            $quiz->status = Quiz::TYPE_DRAFT;
        }

        /**
         * @var PDO $dbh
         */
        $dbh = WebApp::instance()->db;
        // транзакция только в одном месте для примера
        $dbh->beginTransaction();

        try {
            $quiz->save();

            if (empty($quiz_id)) {
                $quiz_id = $dbh->lastInsertId();
            }

            // удаляем все связаные вопросы для пересохранения
            Question::instance()->delete(['quiz_id' => $quiz_id]);

            foreach ($post['Question'] as $question_params) {
                $question           = new Question();
                $question->quiz_id  = $quiz_id;
                $question->question = $question_params['question'];
                $question->type     = isset($question_params['type']) ? 1 : 0;
                $question->required = isset($question_params['required']) ? 1 : 0;
                $question->save();

                $question_id = $dbh->lastInsertId();

                foreach ($question_params['answers'] as $answer_text) {
                    $answer              = new Answer();
                    $answer->answer      = $answer_text;
                    $answer->question_id = $question_id;
                    $answer->save();
                }
            }

            $dbh->commit();

            return ['result' => 'Опрос сохранен'];
        } catch (PDOException $e) {
            $dbh->rollBack();

            return ['error' => ['Ошибка базы']];
        }
    }

    /**
     * Валидация при создании и обновлении опроса
     *
     * @param $post
     *
     * @return array
     */
    protected function validate($post)
    {
        $result    = [];
        $quiz      = isset($post['Quiz']) ? $post['Quiz'] : false;
        $questions = isset($post['Question']) ? $post['Question'] : [];

        // Если пришел пост-запрос, не содержащий ключ Quiz в массиве значений - ошибка
        if (!$quiz) {
            return ['errors' => ['Неизвестная ошибка']];
        }

        // проверка на пустое поле имени опроса
        if (empty($quiz['name'])) {
            $result['errors'][] = 'Не введено название опроса';
        }

        // проверка на количество вопросов
        if (count($questions) < 1) {
            $result['errors'][] = 'Должен быть введен хотя бы один 1 вопрос';
        }

        // проверка на количество ответов в каждом вопросе
        foreach ($questions as $question) {
            if (!isset($question['answers']) || count($question['answers']) < 2) {
                $result['errors'][] = 'Для каждого введенного вопроса, должно быть введено не менее двух ответов';
                break;
            }
        }

        // дополнительная проверка на то что все поля заполнены
        foreach ($questions as $question) {
            if (empty($question['question'])) {
                $result['errors'][] = 'Нужно заполнить все поля';
                break;
            }

            if (isset($question['answers'])) {
                foreach ($question['answers'] as $answer) {
                    if (empty($answer)) {
                        $result['errors'][] = 'Нужно заполнить все поля';
                        break 2;
                    }
                }
            }
        }

        // проверяем что хотя бы один вопрос должен быть обязательным
        $required_not_set = true;

        foreach ($questions as $question) {
            if (isset($question['required']) && $question['required'] == 'on') {
                $required_not_set = false;
            }
        }

        if ($required_not_set) {
            $result['errors'][] = 'Хотя бы один из вопросов должен быть обязательным';
        }

        return $result;
    }

    /**
     * @param string $className
     *
     * @return Quiz
     */
    public static function instance($className = __CLASS__)
    {
        return parent::instance($className);
    }
}