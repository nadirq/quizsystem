<?php
/**
 * @var Quiz $quiz
 * @var Question[] $questions
 * @var Answer[] $answers
 */
?>

<div class="container">
    <div class="row">
        <h5 class="light">Конструктор</h5>
        <p class="light">Для просмотра результатов опроса в разрезе определенных категорий пользователей нужно отметить
            любые ответы, после чего ему сформируется отчет в котором покажутся результаты
            только тех пользователей, которые на ответили аналогичным образом на отмеченные вопросы.</p>
        <p class="light"><a href="/index.php?route=quiz/manager"><< Назад к списку опросов</a></p>
    </div>

    <form action="/index.php?route=result/report" method="post">
        <input type="hidden" value="<?= $quiz->id; ?>" name="Quiz[quiz_id]">

        <?php foreach ($questions as $i => $question) : ?>
            <h5><?= ++$i . '. ' . $question->question; ?></h5>
            <input type="hidden" name="Quiz[id]" value="<?= $quiz->id; ?>">
            <input type="hidden" name="Question[<?= $question->id; ?>][required]"
                   value="<?= $question->required; ?>"/>
            <?php foreach ($answers[$question->id] as $answer) : ?>
                <p>
                    <input type="checkbox" class="filled-in"
                           name="Question[<?= $question->id; ?>][Answers][]"
                           id="Question[<?= $question->id; ?>][Answers][<?= $answer->id; ?>]"
                           value="<?= $answer->id; ?>"'/>
                    <label class="black-text"
                           for="Question[<?= $question->id; ?>][Answers][<?= $answer->id; ?>]">
                        <?= $answer->answer; ?>
                    </label>
                </p>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <button class="btn waves-effect waves-light btn-flat" type="submit" name="action">
            Построить отчет<i class="material-icons right">launch</i>
        </button>
    </form>
</div>
