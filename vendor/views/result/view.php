<?php
/**
 * Представление для построения отчетов
 *
 * @var array $result
 *
 * @see ResultController::view()
 * @see ResultController::report()
 */

?>

<div class="container">
    <h4 class="light">Просмотр результатов опроса</h4>
    <div class="row">
        <div class="col s6">
            <?php foreach ($result['questions'] as $i => $question) : ?>
                <div class="row">
                    <br>
                    <h5 class="light"><?= ++$i; ?>. <?= $question['question'] ?></h5>
                    <?php foreach ($question['answers'] as $answer) : ?>
                        <!--вопрос-->
                        <div class="col s6">
                            <p><?= $answer['answer'] ?></p>
                        </div>
                        <!--счетчик-->
                        <?php $polled = $result['total'] == 0 ? 0 : $answer['count'] / $result['total'] * 100; ?>
                        <div class="col s6">
                            <div class="row" style="margin-bottom: 0;">
                                <div class="red" style="width: 100%" >
                                    <div class="blue recalculate" style="width: 0;" data-polled="<?= $polled; ?>">&nbsp;</div>
                                </div>
                                <div class="col s12">
                                    <p class="light"
                                       style="margin-bottom: 0; padding-bottom: 0;"><?= $answer['count'] ?>
                                        из <?= $result['total'] ?></p>
                                </div>
                            </div>

                        </div>
                        <div class="col s12">
                            <hr style=" border: 0; height: 1px; background-image: linear-gradient(to right, rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.05), rgba(0, 0, 0, 0));">
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<script>
    // рисуем диаграмму с голосами
    $(document).ready(function () {
       recalculate = $('.recalculate');

       recalculate.each(function () {
           new_width = $(this).data('polled');
           $(this).animate({width: new_width+'%'}, 3000, 'swing');
       })

    });
</script>

