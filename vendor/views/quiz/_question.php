<?php
/**
 * Представление для вопроса
 *
 * @var QuizController $this
 * @var Question       $question
 * @var Answer[]       $answers
 *
 * @see QuizController::question()
 * @see QuizController::edit()
 */

$type     = $question->type == 1 ? 'checked="checked"' : '';
$required = $question->required == 1 ? 'checked="checked"' : '';
?>
<div class="question">
    <div class="row">
        <div class="col s6">
            <div class="input-field col s12">
                <h5 class="light green lighten-4">Введите текст вопроса:</h5>
                <input name='Question[<?= $i; ?>][question]'
                       placeholder="Введите текст вашего вопроса"
                       id="Question[<?= $i; ?>][question]"
                       type="text"
                       class="validate"
                       value="<?= $question->question; ?>">
            </div>
            <div class="col s8">
                <p class="lighten-3">Выберите тип ответов</p>
                <div class="switch">
                    <label>Один ответ<input type="checkbox" name="Question[<?= $i; ?>][type]" <?= $type; ?>><span
                                class="lever"></span>Множественный выбор</label>
                </div>
            </div>
            <div class="col s4">
                <p class="lighten-3">Обязательный вопрос</p>
                <div class="switch">
                    <label>Нет<input type="checkbox" name="Question[<?= $i; ?>][required]" <?= $required; ?>><span
                                class="lever"></span>Да</label>
                </div>
            </div>
        </div>
    </div>

    <p> Варианты ответов: </p>
    <?php
    if (!empty($answers)) {
        foreach ($answers[$question->id] as $answer) {
            $this->generatePartial('_answer', ['i' => $i, 'answer' => $answer]);
        }
    }
    ?>

    <a class="waves-effect waves-light btn-flat blue lighten-1 add_answer"
       href="/index.php?route=quiz/answer&i=<?= $i; ?>">
        <i class="material-icons left">add</i>Добавить вариант ответа
    </a>
    <hr>
</div>