<?php
/**
 * Представление для создания опроса
 *
 * @var QuizController $this
 * @var Question[]     $questions
 * @var Answer[]     $answers
 * @var Quiz           $quiz
 *
 * @see QuizController::edit()
 */

?>
<div class="container">
    <div class="row">
        <h5 class="light">Создание/обновление опроса</h5>
        <p class="light">На данной странице можно создать новый опрос.</p>
        <p class="light"><a href="/index.php?route=quiz/manager"><< Назад к списку опросов</a></p>
        <p class="light">Требования и возможности:</p>
        <ul class="collection">
            <li class="collection-item text-lighten-3">
                Опрос может содержать любое количество вопросов
            </li>
            <li class="collection-item text-lighten-3">
                Каждый вопрос может содержать любое количество ответов
            </li>
            <li class="collection-item text-lighten-3">
                Для каждого вопроса нужно ввести минимум два ответа и выбрать тип
            </li>
            <li class="collection-item text-lighten-3">
                Хотя бы один из введенных вопросов должен быть обязательным для ответа
            </li>
        </ul>
    </div>

    <div class="row" id="saving_errors" style="display: none">
        <p class="light">Ошибка сохранения: </p>
        <ul class="collection red-text text-lighten-2">

        </ul>
    </div>
    <div id="result_block" style="display: none">
        <ul class="collection green-text">
            <li class="collection-item green lighten-4"></li>
        </ul>
    </div>

    <form action="" method="post">
        <input type="hidden" value="<?= $quiz->id; ?>" name="Quiz[quiz_id]">
        <div class="row">
            <div class="input-field col s6">
                <h4 class="light">Введите название опроса:</h4>
                <input name='Quiz[name]'
                       placeholder="Введите название вашего опроса"
                       id="Quiz[name]"
                       type="text"
                       class="validate"
                       value="<?= $quiz->name; ?>">
            </div>
        </div>
        <div class="question"></div>
        <?php
        foreach ($questions as $i => $question) {
            $this->generatePartial('_question', ['i' => $i, 'question' => $question, 'answers' => $answers]);
        }
        ?>


        <br>
        <div>
            <a class="waves-effect waves-light btn-flat btn-large green-text" id="add_question"><i
                        class="material-icons right">add</i> Добавить вопрос</a>
            <button class="btn waves-effect waves-light btn-flat" type="submit" name="action">
                Сохранить в черновики<i class="material-icons right">save</i>
            </button>
        </div>
    </form>

</div>

<script>
    $(document).ready(function () {
        i = <?= isset($i) ? ++$i : 0;?>;
        // добавление вопроса
        $('a#add_question').on('click', function () {
            $.ajax({
                type: 'GET',
                url: '/index.php?route=quiz/question&i=' + i,
                success: function (msg) {
                    $('form>div.question').last().append(msg);
                    i++;
                }
            });
        });

        // добавление нового варианта ответа
        $('div.question').on('click', 'a.add_answer', function (event) {
            button = $(this);
            url = button.attr('href');
            event.preventDefault();
            $.ajax({
                type: 'GET',
                url: url,
                success: function (msg) {
                    button.before(msg);
                }
            });
        });

        // действия при отправке формы. Делаем ajax валидацию
        $('form').on('submit', function (event) {
            $.ajax({
                type: "POST",
                url: '/index.php?route=quiz/ajaxSave',
                data: $('form').serialize(),
                success: function (msg) {
                    msg = JSON.parse(msg);

                    if (msg.errors !== undefined) {
                        errors_block = $('#saving_errors');
                        // если есть ошибки - показываем и отменяем отправку формы
                        errors_block.show().find('ul').html('');

                        msg.errors.forEach(function (item, i, arr) {
                            errorRow = '<li class="collection-item red lighten-4">' + item + '</li>';
                            errors_block.find('ul').append(errorRow);
                        });

                        // прокручиваем наверх
                        $('html,body').animate({ scrollTop: 0 }, 1500);
                    } else if (msg.result !== undefined) {
                        // при удачном сохранении переходим в панель управления
                        $(window).attr('location', '/index.php?route=quiz/manager');
                    }
                }
            });

            event.preventDefault();
        });
    });
</script>