<?php
/**
 * Представление для прохождения опроса
 *
 * @var Quiz       $quiz
 * @var Question[] $questions
 * @var Answer[]   $answers
 *
 * @see QuizController::start()
 */
$required_html = '<b class="red-text"> *</b>';
?>

<div class="container">
    <?php if (empty($quiz)) : ?>
        <p class="light">Нет активных опросов</p>
    <?php else: ?>
        <h3 class="header light"><?= $quiz->name; ?></h3>
        <p class="light">Вопросы помеченные <?= $required_html; ?> обязательны для ответа</p>
        <div class="row" id="saving_errors" style="display: none">
            <p class="light">Ошибка обработки результата: </p>
            <ul class="collection red-text text-lighten-2">
            </ul>
        </div>
        <form method="post">
            <!-- Проходим по вопросам -->
            <?php foreach ($questions as $i => $question) : ?>
                <h5><?= ++$i . '. ' . $question->question; ?><?= $question->required == 1 ? $required_html : ''; ?></h5>
                <input type="hidden" name="Quiz[id]" value="<?= $quiz->id; ?>">
                <input type="hidden" name="Question[<?= $question->id; ?>][required]"
                       value="<?= $question->required; ?>"/>

                <!--Проходим по ответам, определяем тип-->
                <?php foreach ($answers[$question->id] as $answer) : ?>
                    <?php $type = $question->type == Question::TYPE_CHECKBOX ? "checkbox" : "radio"; ?>
                    <p>
                        <input type="<?= $type; ?>" class="filled-in"
                               name="Question[<?= $question->id; ?>][Answers][]"
                               id="Question[<?= $question->id; ?>][Answers][<?= $answer->id; ?>]"
                               value="<?= $answer->id; ?>"'/>
                        <label class="black-text"
                               for="Question[<?= $question->id; ?>][Answers][<?= $answer->id ?>]">
                            <?= $answer->answer; ?>
                        </label>
                    </p>
                <?php endforeach; ?>
            <?php endforeach; ?>

            <br>
            <button class="btn waves-effect waves-light btn-flat green lighten-1 btn-large" type="submit" name="action">
                Отправить результаты<i class="material-icons right">launch</i>
            </button>
        </form>
    <?php endif; ?>
</div>
<script>
    // действия при отправке формы. Делаем ajax валидацию
    $('form').on('submit', function (event) {
        $.ajax({
            type: "POST",
            url: '/index.php?route=result/ajaxSave',
            data: $('form').serialize(),
            success: function (msg) {
                msg = JSON.parse(msg);
                if (msg.errors !== undefined) {
                    errors_block = $('#saving_errors');
                    // если есть ошибки - показываем и отменяем отправку формы
                    errors_block.show().find('ul').html('');

                    msg.errors.forEach(function (item, i, arr) {
                        errorRow = '<li class="collection-item red lighten-4">' + item + '</li>';
                        errors_block.find('ul').append(errorRow);
                    });

                    $('html,body').animate({scrollTop: 0}, 'slow');
                } else if (msg.result !== undefined) {
                    $(window).attr('location', '/index.php?route=result/view&quiz_id=<?=$quiz->id?>');
                }
            }
        });

        event.preventDefault();
    });
</script>