<?php
/**
 * Представление для панели управления
 *
 * @var Quiz[] $quizzes
 *
 * @see QuizController::manager()
 */
?>

<div class="container">
    <br>
    <h4 class="header orange-text light">Список опросов</h4>
    <br>
    <div class="row">
        <a href="/index.php?route=quiz/edit">Создать новый опрос</a>
        <?php if (empty($quizzes)) : ?>
            <p>Список опросов пуст. </p>
        <?php else : ; ?>
            <table class="centered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Название опроса</th>
                    <th>Статус</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($quizzes as $quiz) : ?>
                    <tr>
                        <td><?= $quiz->id; ?></td>
                        <td><?= $quiz->name; ?></td>
                        <td><?= Quiz::TYPES[$quiz->status]; ?></td>
                        <td>
                            <?php if ($quiz->status == Quiz::TYPE_DRAFT): ?>
                                <a href="/index.php?route=quiz/edit&id=<?= $quiz->id; ?>" title="Редактировать опрос">
                                    <i class="material-icons">edit</i>
                                </a>
                            <?php endif; ?>
                            <?php if ($quiz->status == Quiz::TYPE_ACTIVE || $quiz->status == Quiz::TYPE_CLOSED): ?>
                                <a href="/index.php?route=result/constructor&id=<?= $quiz->id; ?>"
                                   title="Просмотр результатов опроса">
                                    <i class="material-icons">visibility</i>
                                </a>
                            <?php endif; ?>
                            <?php if (isset($have_active) && $have_active !== true) : ?>
                                <a href="/index.php?route=quiz/activate&id=<?= $quiz->id; ?>"
                                   title="Активировать опрос">
                                    <i class="material-icons">play_circle_outline</i>
                                </a>
                            <?php endif; ?>
                            <?php if ($quiz->status == Quiz::TYPE_ACTIVE): ?>
                                <a href="/index.php?route=quiz/close&id=<?= $quiz->id; ?>" title="Закрыть опрос">
                                    <i class="material-icons">power_off</i>
                                </a>
                            <?php endif; ?>

                            <a href="/index.php?route=quiz/delete&id=<?= $quiz->id; ?>" class="delete_quiz"
                               title="Удалить опрос">
                                <i class="material-icons">delete</i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php endif; ?>
    </div>
</div>

<script>
    $(document).ready(function () {
        // Подтверждение об удалении опроса
        $('a.delete_quiz').on('click', function (event) {
            result = confirm('Вы действительно хотите удалить опрос? Все настройки и результаты будут утеряны.');
            if (!result) {
                event.preventDefault();
            }
        });
    });
</script>