<?php
/**
 * Представление для строки ответа
 *
 * @var int    $i
 * @var string $answer
 *
 * @see QuizController::answer()
 * @see QuizController::edit()
 */
?>

<div class="row">
    <div class="input-field col s6">
        <input name='Question[<?= $i; ?>][answers][]'
               placeholder="Введите вариант ответа"
               id="Question[<?= $i; ?>][answers][]"
               type="text"
               class="validate"
               value="<?= isset($answer) ? $answer->answer : ''; ?>"
        >
    </div>
</div>
