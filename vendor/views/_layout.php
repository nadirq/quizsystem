<?php
/**
 * Основной шаблон
 *
 * @var string $content
 *
 * @see Controller::generate()
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Quiz</title>
    <link href="assets/css/materialize.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="assets/js/jquery-3.1.1.min.js"></script>
    <script src="assets/js/materialize.js"></script>
</head>
<body>
<header>
    <nav class="light-blue lighten-1" role="navigation">
        <div class="nav-wrapper container"><a id="logo-container" href="/index.php?route=quiz/start" class="brand-logo">Quiz</a>
            <ul class="right hide-on-med-and-down">
                <li><a href="/index.php">Пройти активный опрос</a></li>
                <li><a href="/index.php?route=quiz/edit">Создать новый опрос</a></li>
                <li><a href="/index.php?route=quiz/manager">Панель управления</a></li>
            </ul>
        </div>
    </nav>
</header>
<main>

    <?php echo $content; ?>

</main>

<footer class="page-footer grey">
    <div class="footer-copyright">
        <div class="container center">
            Quiz &copy;
        </div>
    </div>
</footer>
</body>
</html>

