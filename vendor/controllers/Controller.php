<?php

/**
 * Class Controller
 */
class Controller
{
    protected $_layout;

    /**
     * Рендер шаблона
     *
     * @param string $view_name Имя представления
     * @param array  $params    Список переменных
     * @param bool   $return    Возвращать или выводить
     */
    public function generate($view_name, array $params = [], $return = false)
    {
        $main_layout = dirname(__DIR__) . '/views/_layout.php';
        if (file_exists($main_layout)) {
            $content = $this->generatePartial($view_name, $params, true);
            $this->processOutput($main_layout, $content);
        }
    }

    /**
     * Рендерит шаблон без подключения в основной шаблон.
     *
     * @param string $view_name Название файла представления
     * @param array  $params    Список переменных
     * @param bool   $return
     *
     * @return string
     * @throws ViewFileNotFound
     */
    public function generatePartial($view_name, array $params = [], $return = false)
    {
        $class_name = get_class($this);
        $controller = strtolower(preg_replace('/(.*)Controller/', '${1}', $class_name));
        $template   = dirname(__DIR__) . '/views/' . $controller . '/' . $view_name . '.php';

        if (file_exists($template)) {
            foreach ($params as $key => $value) {
                $$key = $value;
            }

            // включаем буферизацию вывода
            ob_start();
            include $template;
            // Записываем вывод в переменную и закрываем буферизацию
            $content = ob_get_clean();

            if ($return) {
                return $content;
            } else {
                echo $content;
            }
        } else {
            throw new ViewFileNotFound($template);
        }
    }

    /**
     * @param string $layout  Название основного шаблона
     * @param string $content Сгенерированные дополнительные шаблоны
     */
    protected function processOutput($layout, $content)
    {
        ob_start();
        include($layout);
        $html = ob_get_clean();
        echo $html;
    }

    /**
     * Редирект
     *
     * @param string $url
     */
    protected function redirect($url)
    {
        header('Location: ' . $url);
        exit;
    }
}