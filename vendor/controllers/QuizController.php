<?php

/**
 * Class QuizController
 *
 * TODO реализовать систему доступа к методам
 */
class QuizController extends Controller
{
    /**
     * Создание и обновление опроса
     * Подразумевается что все существующие опросы проходили валидацию и содержат правильные данные и связи
     * Также нет проверки на запрос несуществующего опроса.
     */
    public function edit()
    {
        $answers = [];

        if (isset($_GET['id'])) {
            $quiz      = Quiz::instance()->findByAttributes(['id' => $_GET['id']])[0];
            $questions = Question::instance()->findByAttributes(['quiz_id' => $quiz->id]);

            if (empty($questions)) {
                $questions[] = new Question();
            } else {
                // запрашиваем список ответов
                foreach ($questions as $question) {
                    $question_answers       = Answer::instance()->findByAttributes(['question_id' => $question->id]);
                    $answers[$question->id] = $question_answers;
                }
            }
        } else {
            $quiz      = new Quiz();
            $questions = [];
        }

        $this->generate('edit', [
            'quiz'      => $quiz,
            'questions' => $questions,
            'answers'   => $answers,
        ]);
    }

    /**
     * Управление опросами
     */
    public function manager()
    {
        $quizzes     = Quiz::instance()->findAll();
        $have_active = Quiz::instance()->findByAttributes(['status' => Quiz::TYPE_ACTIVE]);
        $this->generate('manager', [
            'quizzes'     => $quizzes,
            'have_active' => (bool)$have_active,
        ]);
    }

    /**
     * Действие активации опроса
     */
    public function activate()
    {
        // проверяем, есть ли активные опросы
        $have_active = Quiz::instance()->findByAttributes(['status' => Quiz::TYPE_ACTIVE]);

        if (!(bool)$have_active) {
            $quiz         = Quiz::instance()->findByAttributes(['id' => $_GET['id']])[0];
            $quiz->status = Quiz::TYPE_ACTIVE;
            $quiz->save();
        }

        $this->redirect('index.php?route=quiz/manager');
    }

    /**
     * Действие деактивации опроса
     */
    public function close()
    {
        $quiz         = Quiz::instance()->findByAttributes(['id' => $_GET['id']])[0];
        $quiz->status = Quiz::TYPE_CLOSED;
        $quiz->save();
        $this->redirect('index.php?route=quiz/manager');
    }

    /**
     * Действие удаление опроса
     */
    public function delete()
    {
        $quiz = Quiz::instance()->findByAttributes(['id' => $_GET['id']])[0];
        $quiz->delete();
        $this->redirect('/index.php?route=quiz/manager');
    }

    /**
     * Прохождение активного опроса
     */
    public function start()
    {
        $quiz      = Quiz::instance()->findByAttributes(['status' => Quiz::TYPE_ACTIVE]);
        $questions = [];
        $answers   = [];

        if (!empty($quiz)) {
            $quiz      = $quiz[0];
            $questions = Question::instance()->findByAttributes(['quiz_id' => $quiz->id]);

            foreach ($questions as $question) {
                $answers[$question->id] = Answer::instance()->findByAttributes(['question_id' => $question->id]);
            }
        }

        $this->generate('start', ['quiz' => $quiz, 'questions' => $questions, 'answers' => $answers]);
    }

    /**
     * Валидация и сохранение опроса посредсвом ajax
     */
    public function ajaxSave()
    {
        if (!empty($_POST)) {
            $result = Quiz::instance()->saveDraft($_POST);
        } else {
            $result['errors'][] = 'ServiceError: POST array must be provided';
        }

        echo json_encode($result);
    }

    /**
     * Метод выдает html с новым вопросом для ajax-запроса
     */
    public function question()
    {
        $i            = $_GET['i'];
        $question     = new Question();
        $question_row = $this->generatePartial('_question', ['i' => $i, 'question' => $question, 'answers' => []], true);

        echo $question_row;
    }

    /**
     * Метод выдает html с ответом для ajax-запроса
     */
    public function answer()
    {
        $i          = $_GET['i'];
        $answer_row = $this->generatePartial('_answer', ['i' => $i, 'answers' => []], true);

        echo $answer_row;
    }
}