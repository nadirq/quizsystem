<?php

/**
 * Class ResultController
 */
class ResultController extends Controller
{
    /**
     * Просмотр результатов опроса без параметров
     */
    public function view()
    {
        /**
         * @var Result[] $results
         */
        $results     = Result::instance()->findByAttributes(['quiz_id' => $_GET['quiz_id']]);
        $result_data = $this->constructReport($results, $_GET['quiz_id']);

        $this->generate('view', ['result' => $result_data]);
    }

    /**
     * Действие построения условий отчета в разрезе категорий
     */
    public function constructor()
    {
        // запрашиваем все вопросы и ответы
        $quiz      = Quiz::instance()->findByAttributes(['id' => $_GET['id']]);
        $questions = [];
        $answers   = [];

        if (!empty($quiz)) {
            $quiz      = $quiz[0];
            $questions = Question::instance()->findByAttributes(['quiz_id' => $quiz->id]);

            foreach ($questions as $question) {
                $answers[$question->id] = Answer::instance()->findByAttributes(['question_id' => $question->id]);
            }
        }

        $this->generate('constructor', ['quiz' => $quiz, 'questions' => $questions, 'answers' => $answers]);
    }

    /**
     * Действие построения отчета по переданным условиям
     */
    public function report()
    {
        // получаем список уникальных айдишников пользователей, которые подходят под условие
        list($unique_ids, $clear_report) = $this->constructParams($_POST);

        $params = [];

        /**
         * @var Result[] $results
         */
        if (!empty($unique_ids) || $clear_report) {
            foreach ($unique_ids as $unique_id) {
                $params['id'][] = $unique_id;
            }

            $params['quiz_id'] = $_POST['Quiz']['id'];
            $results           = Result::instance()->findByAttributes($params);
        } else {
            $results = [];
        }

        // построение отчета по заданным параметрам
        $result_data = $this->constructReport($results, $_POST['Quiz']['id']);

        $this->generate('view', ['result' => $result_data]);
    }

    /**
     * @param array $results Массив результатов
     * @param int   $quiz_id ID опроса
     *
     * @return array
     */
    protected function constructReport($results, $quiz_id)
    {
        $results_count = 0;
        $unique        = [];

        // считаем количество уникальных результатов по опросу
        foreach ($results as $r) {
            if (!in_array($r->id, $unique)) {
                $unique[] = $r->id;
                $results_count++;
            }
        }

        $result_data          = [];
        $result_data['total'] = $results_count;
        $questions            = Question::instance()->findByAttributes(['quiz_id' => $quiz_id]);

        // бежим по всем вопросам
        foreach ($questions as $i => $question) {
            $result_data['questions'][$i]['question'] = $question->question;
            $answers                                  = Answer::instance()->findByAttributes(['question_id' => $question->id]);
            // бежим по всем ответам вопроса
            foreach ($answers as $j => $answer) {
                $answer_count = 0;

                // подсчитываем количество людей которые выбрали этот ответ
                foreach ($results as $r) {
                    if ($r->question_id == $question->id && $r->answer_id == $answer->id) {
                        $answer_count++;
                    }
                }

                $result_data['questions'][$i]['answers'][$j]['answer'] = $answer->answer;
                $result_data['questions'][$i]['answers'][$j]['count']  = $answer_count;
            }
        }

        return $result_data;
    }

    /**
     * Вычисляет параметры по условиям построения отчета
     *
     * @param array $post
     *
     * @return array
     */
    protected function constructParams($post)
    {
        $unique_ids   = [];
        $clear_report = true; // если не был выбран ни один параметр

        // бежим по каждому вопросу
        foreach ($post['Question'] as $id => $q_data) {

            if (!empty($q_data['Answers'])) {
                // если есть ответы
                $clear_report = false; // если хоть один параметр был выбран - ставим флаг в false
                $params       = [];

                foreach ($q_data['Answers'] as $i => $answer_id) {
                    // Добавляем в условие "ИЛИ" каждый ответ
                    $params['answer_id'][] = $answer_id;
                }

                // добавляем в условие уникальных пользователей
                // таким образом мы получим пулл пользователей, которые ответили также, как было отмечено администратором
                foreach ($unique_ids as $unique_id) {
                    $params['id'][] = $unique_id;
                }

                // промежуточные результаты
                $interval_results = Result::instance()->findByAttributes($params);
                $unique_ids       = [];

                // если совпадений не найдено - выходим, условия выборки не выполнились
                if (empty($interval_results)) {
                    break;
                }

                // Обновляем id пользователей
                foreach ($interval_results as $i_r) {
                    if (!in_array($i_r->id, $unique_ids)) {
                        $unique_ids[] = $i_r->id;
                    }
                }
            } else {
                // проверяем что флаг ни разу не был false
                $clear_report = $clear_report && true;
            }
        }

        return [$unique_ids, $clear_report];
    }

    /**
     * Сохранение результатов голосования
     */
    public function ajaxSave()
    {
        if (!empty($_POST)) {
            $result = Result::instance()->savePoll($_POST);
        } else {
            $result['errors'][] = 'ServiceError: POST array must be provided';
        }

        echo json_encode($result);
    }
}