<?php

/**
 * Class ViewFileNotFound
 */
class ViewFileNotFound extends Exception
{
    public function __construct($template)
    {
        $message = 'Template file not found: ' . $template;
        parent::__construct($message);
    }
}