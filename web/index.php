<?php

$config = require_once(dirname(__DIR__) . '/config.php');

// включаем автозагрузку классов
require_once (dirname(__DIR__). '/autoload.php');
spl_autoload_register(['AutoLoader', 'load']);

//TODO обработчик ошибок

WebApp::createInstance($config);