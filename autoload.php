<?php

/**
 * Class AutoLoader
 */
class AutoLoader
{
    private static $paths = [
        '/controllers',
        '/models',
        '/common',
        '/exceptions'
    ];

    /**
     * @param $className
     *
     * @throws Exception
     */
    public static function load($className)
    {
        $include = '';

        foreach (self::$paths as $path) {
            $include .= PATH_SEPARATOR . __DIR__ . '/vendor' . $path;
        }

        set_include_path(get_include_path() . $include);

        $includes = explode(PATH_SEPARATOR, get_include_path());

        foreach ($includes as $include) {
            if (file_exists($include . '/' . $className . '.php')) {
                require_once($include . '/' . $className . '.php');
                return;
            }
        }

        throw new Exception('Class ' . $className . ' not found');
    }
}